package cz.sample.rest.controller;

import cz.sample.rest.dto.FSObject;
import cz.sample.rest.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FSController {

    @Autowired
    private FileService _fileService;

    @RequestMapping(value = "fs/test", method = RequestMethod.GET)
    public String test() {
        return "OK";
    }

    @RequestMapping(value = "/fs/list", method = RequestMethod.GET)
    @ResponseBody
    public List<FSObject> list(@RequestParam(value = "path", required = false, defaultValue = "c:/") final String path) {
        return _fileService.getDirList(new java.io.File(path).toPath());
    }

}
