package cz.sample.rest.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = { "cz.sample.rest.service" })
public class ServiceConfig {
}
