package cz.sample.rest.service;

import com.google.common.base.Preconditions;
import cz.sample.rest.dto.Dir;
import cz.sample.rest.dto.FSObject;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class FileServiceImpl implements FileService {

	@Override
	public List<FSObject> getDirList(final Path path) {
		Preconditions.checkNotNull(path, "path is null");

		final File[] files = path.toFile().listFiles();

		return Arrays.stream(files).map(FileServiceImpl::convert).collect(Collectors.toList());
	}

	private static FSObject convert(final File file) {
		if (file.isDirectory()) {
			final Dir dir = new Dir();
			setFSObject(file, dir);
			return dir;
		} else {
			final cz.sample.rest.dto.File f = new cz.sample.rest.dto.File();
			setFSObject(file, f);
			try {
				f.setSize(Files.size(file.toPath()));
			} catch (IOException e) {
				throw new RuntimeException("getting file size: path=" + file.getAbsolutePath(), e);			}
			return f;
		}
	}

	private static void setFSObject(final File file, final FSObject fso) {
		fso.setPath(file.getAbsolutePath());
		fso.setHidden(file.isHidden());
	}
}
