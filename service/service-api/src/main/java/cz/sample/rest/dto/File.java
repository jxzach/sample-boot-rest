package cz.sample.rest.dto;

public class File extends FSObject {

	private long _size;

	public File() {
		super(false);
	}

	public long getSize() {
		return _size;
	}

	public void setSize(final long size) {
		_size = size;
	}
}
