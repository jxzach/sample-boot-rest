package cz.sample.rest.service;

import cz.sample.rest.dto.FSObject;

import java.nio.file.Path;
import java.util.List;

public interface FileService {

	List<FSObject> getDirList(final Path path);

}
