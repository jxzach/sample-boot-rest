package cz.sample.rest.dto;

public abstract class FSObject {

    private boolean _dir;
    private String _path;
    private boolean _hidden;

    public FSObject(final boolean dir) {
        _dir = dir;
    }

    public boolean isDir() {
        return _dir;
    }

    public String getPath() {
        return _path;
    }

    public void setPath(final String path) {
        _path = path;
    }

    public boolean isHidden() {
        return _hidden;
    }

    public void setHidden(final boolean hidden) {
        _hidden = hidden;
    }
}
